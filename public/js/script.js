document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems);
});

var sidenavStatus = false;
document.addEventListener('DOMContentLoaded', function() {
	var sidenav = document.querySelectorAll('.sidenav');
	var sidenavInstance = M.Sidenav.init(sidenav);
	// var dropdown = document.querySelectorAll('.dropdown-trigger');
    // var dropdownInstance = M.Dropdown.init(dropdown);
	// var tooltip = document.querySelectorAll('.tooltipped');
    // var tooltipInstance = M.Tooltip.init(tooltip);
	document.onkeydown = function(evt) {
        var sidenav = document.getElementById('mobile-demo');
		var instance = M.Sidenav.getInstance(sidenav);
		evt = evt || window.event;
		var isEscape = false;
		if ("key" in evt) {
			isEscape = (evt.key === "Escape" || evt.key === "Esc");
		} else {
			isEscape = (evt.keyCode === 27);
		}
		if (isEscape && sidenavStatus == false) {
			instance.open();
			sidenavStatus = true;
		} else {
			sidenavStatus = false;
			instance.close();
		}
	};
});
function keyPress (e) {
	if(e.key === "Escape") {
		console.log("esc")
	}
}