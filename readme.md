# Hacked Movies API
Déploiement sur [**Hacked Movies API**](http://hacked-movies-api.git-projects.xyz/)

![](public/images/logos/hacked-movies-laravel-readme.png)


[![Laravel 5.8](https://img.shields.io/static/v1?label=Laravel&message=v5.8&color=ff2d20&style=flat-square&logo=laravel&logoColor=ff2d20)](https://laravel.com)
[![PHP 7.2](https://img.shields.io/static/v1?label=PHP&message=v7.1&color=777bb4&style=flat-square&logo=php&logoColor=777bb4)](https://www.php.net)
[![NodeJS 11.15](https://img.shields.io/static/v1?label=NodeJS&message=v5.8&color=339933&style=flat-square&logo=node.js&logoColor=339933)](https://nodejs.org/en)
[![Composer 1.8](https://img.shields.io/static/v1?label=Laravel&message=v5.8&color=ff2d20&style=flat-square&logo=laravel&logoColor=ff2d20)](https://getcomposer.org)

## Routes

**GET** List of all films, order by title  
`https://hacked-movies-api.git-projects.xyz/api/movies`

**GET** List of _id film  
`https://hacked-movies-api.git-projects.xyz/api/movies/_id`

## Déployer en local

```bash
# Créer la base de données sur phpMyAdmin
# Créer .env et le remplir avec les informations de phpMyAdmin
cp .env.example .env
vim .env
```
>DB_DATABASE=`<database>`  
>DB_USERNAME=`<username>`  
>DB_PASSWORD=`<password>`  

```bash
# Télécharger les dépendances de Laravel
composer install
# dépendances de NodeJS
yarn
# Générer la clé pour .env
php artisan key:generate
# Réaliser la migration de la base de données et la remplir
php artisan migrate:fresh --seed
# compilation des assets
yarn dev
```

Si vous ne souhaitez pas mettre en place un VHost, exécutez la commande suivante :

```bash
php artisan serve
```

## VHost : Apache

### Pour Windows (XAMPP)

Ajouter la configuration suivante à la fin du fichier de VHost de XAMPP situé par défaut dans `C:\xampp\apache\conf\extra\httpd-vhosts.conf`

```apacheconf
<VirtualHost *:80>
    ServerName hacked-movies-api.localhost

    DocumentRoot "C:/workspace/proweb/public"

    <Directory  "C:/workspace/hacked-movies-api">
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
    <Directory  "C:/workspace/hacked-movies-api/public">
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
```

Ajouter la configuration suivante à la fin du le fichier `hosts` de Windows situé dans `C:\Windows\System32\drivers\etc\hosts` (à ouvrir avec un IDE)

```bash
127.0.0.1	hacked-movies-api.localhost
::1	hacked-movies-api.localhost
```

Lors de l'enregistrement des modifications, Windows devrait vous demander de le faire en administrateur, validez simplement et réenregistrez. Redémarrer Apache dans XAMPP et le serveur sera disponible à l'url [**http://hacked-movies-api.localhost**](http://hacked-movies-api.localhost)
