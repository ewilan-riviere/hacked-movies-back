<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'files';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name',
        'file_weight',
    ];

    public $timestamps = false;

    public function formats()
    {
        return $this->belongsToMany(Format::class);
    }
    public function audio()
    {
        return $this->belongsToMany(Audio::class);
    }
    public function subtitles()
    {
        return $this->belongsToMany(Subtitle::class);
    }
}
