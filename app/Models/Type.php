<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'types';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_name',
    ];

    public $timestamps = false;

    public function movies()
    {
        return $this->belongsToMany(Movie::class);
    }
    
    public function franchise()
    {
        return $this->belongsToMany(Franchise::class);
    }
    public function season()
    {
        return $this->belongsToMany(Season::class);
    }
}
