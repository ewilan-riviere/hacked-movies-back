<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Movie extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movies';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_fr',
        'title_en',
        'poster',
        'description',
        'release_year',
        'duration',
        'longest_version',
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'title_en' => 'non-indiqué',
        'poster' => 'image',
        'description' => 'non-indiqué',
        'duration' => 'inconnu',
    ];

    public $timestamps = false;
    
    /**
     * Get the user that owns the task.
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    public function directors()
    {
        return $this->belongsToMany(Director::class);
    }
    public function actors()
    {
        return $this->belongsToMany(Actor::class);
    }
    public function scriptwriters()
    {
        return $this->belongsToMany(Scriptwriter::class);
    }
    public function types()
    {
        return $this->belongsToMany(Type::class)->with('Franchise','Season.Status');
    }
    public function producers()
    {
        return $this->belongsToMany(Producer::class);
    }
    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class, "file_id")->with('Formats','Audio','Subtitles');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, "season_id")->with('Status');
    }

    // Accessors

    public function getPosterAttribute($value) {
        return config('app.url').'/storage/posters/'.$value;
    }

    // Mutators
    
    public function setSlugAttribute() {
        return Str::slug($this->title_en);
    }
}
