<?php

use App\Models\Movie;

function movies() {
    $result = Movie::with(
        'directors',
        'scriptwriters',
        'producers',
        'countries',
        'actors',
        'tags',
        'types',
        'file'
    );

    return $result;
}
