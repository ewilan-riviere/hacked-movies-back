<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $tasks = $user->tasks()->get(); 

        return response()->json($tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $task = new Task;

        $task = $user->tasks()->create([
            'title' => $request->title,
            'description' => $request->description,
            'state' => $request->state,
        ]);

        return response()->json($task);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $user = Auth::user();
        $task = $user->tasks()->where('id', $id)->first();
        
        if ($task) {
            return response()->json($task, 200);
        } else {
            return response()->json(['error'=>'Unauthorized'], 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $user = Auth::user();
        
        $task = $user->tasks()->where('id', $id)->first();

        if ($task) {
            $request->title ? $task->title = $request->title : '';
            $request->description ? $task->description = $request->description : '';
            $request->state ? $task->state = $request->state : '';
            $task->save();

            return response()->json($task, 200);
        } else {
            return response()->json(['error'=>'Unauthorized'], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $user = Auth::user();
        
        $task = $user->tasks()->where('id', $id)->first();

        if ($task) {
            if($task->delete()) {
                return response()->json('success', 200);
            }
        } else {
            return response()->json(['error'=>'Unauthorized'], 401);
        }
    }
}
