<?php

use Illuminate\Database\Seeder;

class AudioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('audio')->insert([
            [
                'audio_name' => 'Anglais',
            ],
            [
                'audio_name' => 'Français',
            ],
        ]);

        DB::table('audio_file')->insert([
            [
                'vo' => true,
                'file_id' => 1,
                'audio_id' => 1,
            ],
            [
                'vo' => false,
                'file_id' => 1,
                'audio_id' => 2,
            ],
        ]);
    }
}
