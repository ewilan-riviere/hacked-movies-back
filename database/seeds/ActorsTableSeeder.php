<?php

use Illuminate\Database\Seeder;

class ActorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actors')->insert([
            [
                'actor_name' => 'Keanu Reeves',
            ],
            [
                'actor_name' => 'Laurence Fishburne',
            ],
            [
                'actor_name' => 'Carrie-Anne Moss',
            ],
            [
                'actor_name' => 'Hugo Weaving',
            ],
            [
                'actor_name' => 'Viggo Mortensen',
            ],
        ]);

        DB::table('actor_movie')->insert([
            [
                'role' => 'Néo',
                'movie_id' => 1,
                'actor_id' => 1,
            ],
            [
                'role' => 'Morpheus',
                'movie_id' => 1,
                'actor_id' => 2,
            ],
            [
                'role' => 'Trinity',
                'movie_id' => 1,
                'actor_id' => 3,
            ],
            [
                'role' => 'Agent Smith',
                'movie_id' => 1,
                'actor_id' => 4,
            ],
        ]);
    }
}
