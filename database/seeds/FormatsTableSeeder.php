<?php

use Illuminate\Database\Seeder;

class FormatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formats')->insert([
            [
                'format_name' => 'avi',
            ],
            [
                'format_name' => 'mkv',
            ],
        ]);

        DB::table('file_format')->insert([
            [
                'file_id' => 1,
                'format_id' => 1,
            ],
            [
                'file_id' => 1,
                'format_id' => 2,
            ],
        ]);
    }
}
