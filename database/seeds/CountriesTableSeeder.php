<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            [
                'country_name' => 'Australien',
            ],
            [
                'country_name' => 'Américain',
            ]
        ]);

        DB::table('country_movie')->insert([
            [
                'movie_id' => 1,
                'country_id' => 1,
            ],
        ]);
    }
}
