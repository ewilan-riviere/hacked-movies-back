<?php

use Illuminate\Database\Seeder;

class SubtitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subtitles')->insert([
            [
                'subtitle_name' => 'français',
            ],
            [
                'subtitle_name' => 'anglais',
            ],
        ]);

        DB::table('file_subtitle')->insert([
            [
                'file_id' => 1,
                'subtitle_id' => 1,
            ],
            [
                'file_id' => 1,
                'subtitle_id' => 2,
            ],
        ]);
    }
}
