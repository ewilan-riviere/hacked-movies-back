<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function recurseCopy($src,$dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recurseCopy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
    public function directoryToStorage($dir) {
		$database_files = database_path('seeds/storage/');
		$src = $database_files.$dir;
		$dst = storage_path('app/public/'.$dir);
        $this->recurseCopy($src,$dst);
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->directoryToStorage('posters');

        $this->call(UsersTableSeeder::class);
        $this->call(FranchisesTableSeeder::class);
        $this->call(FileTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(SeasonsTableSeeder::class);
        $this->call(MoviesTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(DirectorsTableSeeder::class);
        $this->call(ScriptwriterTableSeeder::class);
        $this->call(ProducersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(ActorsTableSeeder::class);
        $this->call(FormatsTableSeeder::class);
        $this->call(AudioTableSeeder::class);
        $this->call(SubtitlesTableSeeder::class);
    }
}
