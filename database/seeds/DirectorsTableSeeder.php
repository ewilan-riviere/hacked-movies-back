<?php

use Illuminate\Database\Seeder;

class DirectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directors')->insert([
            [
                'director_name' => 'Lily Wachowski',
            ],
            [
                'director_name' => 'Lana Wachowski',
            ],
        ]);

        DB::table('director_movie')->insert([
            [
                'movie_id' => 1,
                'director_id' => 1,
            ],
            [
                'movie_id' => 1,
                'director_id' => 2,
            ],
        ]);
    }
}
