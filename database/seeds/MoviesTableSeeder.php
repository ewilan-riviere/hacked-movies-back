<?php

use Illuminate\Database\Seeder;
use App\Models\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movies = [
            // Matrix 
            [
                'title_fr' => 'Matrix',
                'title_en' => 'The Matrix',
                'slug' => '',
                'poster' => 'franchise/matrix/the-matrix.jpg',
                'description' => 'Thomas A. Anderson, un jeune informaticien connu dans le monde du hacking sous le pseudonyme de Neo, est contacté via son ordinateur par ce qu’il pense être un groupe de hackers. Ils lui font découvrir que le monde dans lequel il vit n’est qu’un monde virtuel dans lequel les êtres humains sont gardés inconsciemment sous contrôle.<br>Morpheus, le capitaine du Nebuchadnezzar, contacte Néo et pense que celui-ci est l’Élu qui peut libérer les êtres humains du joug des machines et prendre le contrôle de la matrice (selon ses croyances et ses convictions).',
                'release_year' => 1999,
                'duration' => 136,
                'longest_version' => true,
                'file_id' => 1,
            ],
            // Seigneur des Anneaux : La communauté de l'anneau 
            [
                'title_fr' => 'Le Seigneur des anneaux : La Communauté de l\'anneau',
                'title_en' => 'The Lord of The Rings: The Fellowship of the Ring',
                'slug' => '',
                'poster' => 'franchise/the-lord-of-the-rings/the-lord-of-the-rings-the-fellowship-of-the-ring.jpg',
                'description' => '',
                'release_year' => 2001,
                'duration' => 178,
                'longest_version' => true,
                'file_id' => null,
            ],
            // Star Wars : Un nouvel espoir 
            [
                'title_fr' => 'Star Wars, épisode IV : Un nouvel espoir',
                'title_en' => 'Star Wars: Episode IV – A New Hope',
                'slug' => '',
                'poster' => 'franchise/star-wars/star-wars-episode-iv-a-new-hope.jpg',
                'description' => '',
                'release_year' => 1977,
                'duration' => 121,
                'longest_version' => true,
                'file_id' => null,
            ],
            // Carnival Row  
            [
                'title_fr' => 'Carnival Row',
                'title_en' => 'Carnival Row',
                'slug' => '',
                'poster' => 'show/carnival-row/carnival-row.jpg',
                'description' => '',
                'release_year' => 2019,
                'duration' => null,
                'longest_version' => true,
                'file_id' => null,
            ],
            // Breaking Bad  
            [
                'title_fr' => 'Breaking Bad',
                'title_en' => 'Breaking Bad',
                'slug' => '',
                'poster' => 'show/breaking-bad/breaking-bad.jpg',
                'description' => '',
                'release_year' => 2008,
                'duration' => null,
                'longest_version' => true,
                'file_id' => null,
            ],
            // Westworld  
            [
                'title_fr' => 'Westworld',
                'title_en' => 'Westworld',
                'slug' => '',
                'poster' => 'show/westworld/westworld.jpg',
                'description' => '',
                'release_year' => 2016,
                'duration' => null,
                'longest_version' => true,
                'file_id' => null,
            ],
            // The Boys  
            [
                'title_fr' => 'The Boys',
                'title_en' => 'The Boys',
                'slug' => '',
                'poster' => 'show/the-boys/the-boys.jpg',
                'description' => '',
                'release_year' => 2019,
                'duration' => null,
                'longest_version' => true,
                'file_id' => null,
            ],
            // Game of Thrones  
            [
                'title_fr' => 'Game of Thrones',
                'title_en' => 'Game of Thrones',
                'slug' => '',
                'poster' => 'show/game-of-thrones/game-of-thrones.jpg',
                'description' => '',
                'release_year' => 2011,
                'duration' => null,
                'longest_version' => true,
                'file_id' => null,
            ],
            // Warcraft  
            [
                'title_fr' => 'Warcraft : Le Commencement',
                'title_en' => 'Warcraft: The Beginning',
                'slug' => '',
                'poster' => 'franchise/warcraft/warcraft-the-beginning.jpg',
                'description' => '',
                'release_year' => 2016,
                'duration' => 123,
                'longest_version' => true,
                'file_id' => null,
            ],
            // Star Trek (film, 2009)  
            [
                'title_fr' => 'Star Trek (2009)',
                'title_en' => 'Star Trek',
                'slug' => '',
                'poster' => 'franchise/star-trek/star-trek-2009.jpg',
                'description' => '',
                'release_year' => 2009,
                'duration' => 127,
                'longest_version' => true,
                'file_id' => null,
            ],
            // Default 
            // [
            //     'title_fr' => 'TitleFR',
            //     'title_en' => 'TitleEN',
            //      'slug' => '',
            //      'poster' => 'default-poster.jpg',
            //     'description' => '',
            //     'release_year' => null,
            //     'duration' => null,
            //     'longest_version' => true,
            //     'file_id' => null,
            // ],
        ];

        foreach ($movies as $key => $movie) {
            Movie::create($movie);
        }
    }
}
