<?php

use Illuminate\Database\Seeder;

class ScriptwriterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scriptwriters')->insert([
            [
                'scriptwriter_name' => 'Georges Lucas',
            ],
            [
                'scriptwriter_name' => 'Lily Wachowski',
            ],
            [
                'scriptwriter_name' => 'Lana Wachowski',
            ],
        ]);

        DB::table('movie_scriptwriter')->insert([
            [
                'movie_id' => 3,
                'scriptwriter_id' => 1,
            ],
            [
                'movie_id' => 1,
                'scriptwriter_id' => 2,
            ],
            [
                'movie_id' => 1,
                'scriptwriter_id' => 3,
            ],
        ]);
    }
}
