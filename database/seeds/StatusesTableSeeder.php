<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [
                'status_name' => 'En cours',
            ],
            [
                'status_name' => 'Terminée',
            ],
            [
                'status_name' => 'Arrêtée',
            ],
        ]);
    }
}
