<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            [
                'tag_name' => 'Science-fiction',
            ],
            [
                'tag_name' => 'Cyberpunk',
            ],
            [
                'tag_name' => 'Fantasy',
            ],
            [
                'tag_name' => 'Historique',
            ],
            [
                'tag_name' => 'Policier',
            ],
            [
                'tag_name' => 'Anticipation',
            ],
            [
                'tag_name' => 'VOSTFR',
            ],
        ]);

        DB::table('movie_tag')->insert([
            [
                'movie_id' => 1,
                'tag_id' => 1,
            ],
            [
                'movie_id' => 1,
                'tag_id' => 6,
            ],
            [
                'movie_id' => 1,
                'tag_id' => 2,
            ],
        ]);
    }
}
