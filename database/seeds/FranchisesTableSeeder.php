<?php

use Illuminate\Database\Seeder;

class FranchisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('franchises')->insert([
            [
                'franchise_name' => 'Matrix',
            ],
        ]);
    }
}
