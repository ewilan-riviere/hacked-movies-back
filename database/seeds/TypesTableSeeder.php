<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            [
                'type_name' => 'Film',
            ],
            [
                'type_name' => 'Franchise',
            ],
            [
                'type_name' => 'Série',
            ],
        ]);

        DB::table('movie_type')->insert([
            [
                'movie_id' => 1,
                'type_id' => 2,
            ],
            [
                'movie_id' => 4,
                'type_id' => 3,
            ],
        ]);

        DB::table('franchise_type')->insert([
            [
                'franchise_id' => 1,
                'type_id' => 2,
            ],
        ]);

        DB::table('season_type')->insert([
            [
                'season_id' => 1,
                'type_id' => 3,
            ],
        ]);
    }
}
