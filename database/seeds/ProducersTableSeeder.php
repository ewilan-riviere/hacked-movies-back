<?php

use Illuminate\Database\Seeder;

class ProducersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('producers')->insert([
            [
                'producer_name' => 'Warner Bros.',
            ],
            [
                'producer_name' => 'Village Roadshow Pictures',
            ]
        ]);

        DB::table('movie_producer')->insert([
            [
                'movie_id' => 1,
                'producer_id' => 1,
            ],
            [
                'movie_id' => 1,
                'producer_id' => 2,
            ],
        ]);
    }
}
