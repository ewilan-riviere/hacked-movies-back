<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
        * Tags
        *
        * @return void
        */
        Schema::create('movie_tag', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("movie_tag", function (Blueprint $table) {
            $table->integer("movie_id")->unsigned()->index();
            $table->foreign("movie_id")->references("id")->on("movies")->onDelete("cascade");
            $table->integer("tag_id")->unsigned()->index();
            $table->foreign("tag_id")->references("id")->on("tags")->onDelete("cascade");
        });

        /**
        * Directors
        *
        * @return void
        */
        Schema::create('director_movie', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("director_movie", function (Blueprint $table) {
            $table->integer("movie_id")->unsigned()->index();
            $table->foreign("movie_id")->references("id")->on("movies")->onDelete("cascade");
            $table->integer("director_id")->unsigned()->index();
            $table->foreign("director_id")->references("id")->on("directors")->onDelete("cascade");
        });

        /**
        * Actors
        *
        * @return void
        */
        Schema::create('actor_movie', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->string('role')->nullable();
            $table->timestamps();
        });
        Schema::table("actor_movie", function (Blueprint $table) {
            $table->integer("movie_id")->unsigned()->index();
            $table->foreign("movie_id")->references("id")->on("movies")->onDelete("cascade");
            $table->integer("actor_id")->unsigned()->index();
            $table->foreign("actor_id")->references("id")->on("actors")->onDelete("cascade");
        });

        /**
        * Scriptwriters
        *
        * @return void
        */
        Schema::create('movie_scriptwriter', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("movie_scriptwriter", function (Blueprint $table) {
            $table->integer("movie_id")->unsigned()->index();
            $table->foreign("movie_id")->references("id")->on("movies")->onDelete("cascade");
            $table->integer("scriptwriter_id")->unsigned()->index();
            $table->foreign("scriptwriter_id")->references("id")->on("scriptwriters")->onDelete("cascade");
        });

        /**
        * Producers
        *
        * @return void
        */
        Schema::create('movie_producer', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("movie_producer", function (Blueprint $table) {
            $table->integer("movie_id")->unsigned()->index();
            $table->foreign("movie_id")->references("id")->on("movies")->onDelete("cascade");
            $table->integer("producer_id")->unsigned()->index();
            $table->foreign("producer_id")->references("id")->on("producers")->onDelete("cascade");
        });

        /**
        * Countries
        *
        * @return void
        */
        Schema::create('country_movie', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("country_movie", function (Blueprint $table) {
            $table->integer("movie_id")->unsigned()->index();
            $table->foreign("movie_id")->references("id")->on("movies")->onDelete("cascade");
            $table->integer("country_id")->unsigned()->index();
            $table->foreign("country_id")->references("id")->on("countries")->onDelete("cascade");
        });

        /**
        * Type
        *
        * @return void
        */
        Schema::create('movie_type', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("movie_type", function (Blueprint $table) {
            $table->integer("movie_id")->unsigned()->index();
            $table->foreign("movie_id")->references("id")->on("movies")->onDelete("cascade");
            $table->integer("type_id")->unsigned()->index();
            $table->foreign("type_id")->references("id")->on("types")->onDelete("cascade");
        });

        Schema::create('franchise_type', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("franchise_type", function (Blueprint $table) {
            $table->integer("franchise_id")->unsigned()->index();
            $table->foreign("franchise_id")->references("id")->on("franchises")->onDelete("cascade");
            $table->integer("type_id")->unsigned()->index();
            $table->foreign("type_id")->references("id")->on("types")->onDelete("cascade");
        });

        Schema::create('season_type', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("season_type", function (Blueprint $table) {
            $table->integer("season_id")->unsigned()->index();
            $table->foreign("season_id")->references("id")->on("seasons")->onDelete("cascade");
            $table->integer("type_id")->unsigned()->index();
            $table->foreign("type_id")->references("id")->on("types")->onDelete("cascade");
        });

        /**
        * One to Many
        *
        * @return void
        */
        /* ********************************************************************************** */
        
        /**
        * File
        *
        * @return void
        */
        Schema::table("movies", function (Blueprint $table) {
            $table->integer("file_id")->unsigned()->nullable()->index();
            $table->foreign("file_id")->references("id")->on("files")->onDelete("cascade");
        });

        /* ********************************************************************************** */
        /**
        * Formats
        *
        * @return void
        */
        Schema::create('file_format', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("file_format", function (Blueprint $table) {
            $table->integer("file_id")->unsigned()->index();
            $table->foreign("file_id")->references("id")->on("files")->onDelete("cascade");
            $table->integer("format_id")->unsigned()->index();
            $table->foreign("format_id")->references("id")->on("formats")->onDelete("cascade");
        });

        /**
        * Audio
        *
        * @return void
        */
        Schema::create('audio_file', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->boolean('vo')->nullable();
            $table->timestamps();
        });
        Schema::table("audio_file", function (Blueprint $table) {
            $table->integer("file_id")->unsigned()->index();
            $table->foreign("file_id")->references("id")->on("files")->onDelete("cascade");
            $table->integer("audio_id")->unsigned()->index();
            $table->foreign("audio_id")->references("id")->on("audio")->onDelete("cascade");
        });

        /**
        * Subtitles
        *
        * @return void
        */
        Schema::create('file_subtitle', function (Blueprint $table) {
            $table->unsignedInteger('id')->nullable();
            $table->timestamps();
        });
        Schema::table("file_subtitle", function (Blueprint $table) {
            $table->integer("file_id")->unsigned()->index();
            $table->foreign("file_id")->references("id")->on("files")->onDelete("cascade");
            $table->integer("subtitle_id")->unsigned()->index();
            $table->foreign("subtitle_id")->references("id")->on("subtitles")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
