<!-- OpenGraph -->
<meta
    property="og:url"
    content="https://hacked-movies-api.git-projects.xyz/"
>
<meta
    property="og:title"
    content="Hacked Movies API"
>
<meta
    property="og:description"
    content="The API of Hacked Movies App to display hacked movies."
>
<meta
    property="og:image"
    content="https://hacked-movies-api.git-projects.xyz/images/logos/hacked-movies-laravel.png"
>
<meta
    property="og:type"
    content="website"
>
<meta
    property="og:site_name"
    content="Hacked Movies"
>