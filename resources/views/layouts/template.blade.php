<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @include('layouts.open-graph')

        <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" href="{{ asset('images/logos/hacked-movies-laravel.png') }}" type="image/x-icon">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        {{-- Mateirliaze Icons  --}}
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>

    <main>
        @yield('content')
    </main>

    </body>
    <script src="{{ asset('js/app.js') }}"></script>
</html>