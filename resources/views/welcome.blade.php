@extends('layouts.template')

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">
            <img src="{{ asset('images/logos/hacked-movies-laravel.png') }}" alt="">
            <div class="title pirate-font m-b-md">
                Hacked Movies API
            </div>

            <div class="links">
                {{-- <a href="{{ url('/about') }}">
                    <b>About</b>
                </a> --}}
            </div>
        </div>
    </div>

@endsection

