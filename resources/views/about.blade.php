@extends('layouts.template')

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="m-b-md">
                Hacked Movies API is a free-use API to display hacked movies.
                <div class="mt-4">
                    GitLab repositories:
                    <div class="mt-3 d-flex justify-content-around">
                        <a class="waves-effect waves-light btn"
                            target="_blank"
                            href="https://gitlab.com/EwieFairy/hacked-movies-api">
                            <i class="material-icons left">
                                <i class="fab fa-gitlab"></i>
                            </i>
                            <b>Hacked Movies API</b>
                        </a>

                        <a class="waves-effect waves-light btn"
                            target="_blank"
                            href="https://gitlab.com/EwieFairy/hacked-movies">
                            <i class="material-icons left">
                                <i class="fab fa-gitlab"></i>
                            </i>
                            <b>Hacked Movies</b>
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="links">
                <a class="waves-effect waves-light btn" href="{{ url('/') }}">
                    <i class="material-icons left">arrow_left</i>
                    Back
                </a>
            </div>
        </div>
    </div>
    {{-- <div class="flex-center position-ref full-height">
        <div class="content">
            Hacked Movies API is a free-use API to display hacked movies.
            <div class="mt-2">
                Features:
                <ul>
                    <li>Movie</li>
                    <li>Release year</li>
                    <li>Duration</li>
                </ul>
            </div>
            <div class="mt-4">
                GitLab repositories:
                <div class="mt-3 d-flex justify-content-around">
                    <a class="waves-effect waves-light btn"
                        target="_blank"
                        href="https://gitlab.com/EwieFairy/hacked-movies-api">
                        <i class="material-icons left">
                            <i class="fab fa-gitlab"></i>
                        </i>
                        <b>Hacked Movies API</b>
                    </a>

                    <a class="waves-effect waves-light btn"
                        target="_blank"
                        href="https://gitlab.com/EwieFairy/hacked-movies">
                        <i class="material-icons left">
                            <i class="fab fa-gitlab"></i>
                        </i>
                        <b>Hacked Movies</b>
                    </a>
                </div>
            </div>
        </div>
    </div> --}}

@endsection